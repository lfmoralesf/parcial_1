package servicios;

import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.poli.devweb.model.entities.Consultor;
import com.poli.devweb.model.entities.Cotizacion;
import com.poli.devweb.model.entities.Servicio;
import com.poli.devweb.model.entities.Usuario;
import com.poli.devweb.repositories.CotizacionRepository;
import com.poli.devweb.repositories.ServicioRepository;
import com.poli.devweb.repositories.UsuarioRepository;
@RestController
public class CotizacionController {
	
	@Autowired
	private CotizacionRepository cotizacionRepository;



	@RequestMapping(value="/añadirCotizacion", method = RequestMethod.POST)
    public String consultarServiciosPorConsultor(@RequestBody Cotizacion cotizacion) {//REST Endpoint.
		cotizacionRepository.save(cotizacion);
		return"Cotizacion guardada";
    }
	
	@RequestMapping(value="/getCotizacionByConsultor", method = RequestMethod.POST)
    public Cotizacion consultarCotizacionByConsultor(@RequestBody Consultor consultor) {//REST Endpoint.
		return cotizacionRepository.findByConsultor(consultor);
    	
    }
	
	@RequestMapping(value="/getCotizacionByServicio", method = RequestMethod.POST)
    public Cotizacion consultarCotizacionByServicio(@RequestBody Servicio servicio) {//REST Endpoint.
		return cotizacionRepository.findByServicio(servicio);
    	
    }

	
}
