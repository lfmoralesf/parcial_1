package servicios;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.poli.devweb.model.entities.Usuario;
import com.poli.devweb.repositories.UsuarioRepository;
@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@RequestMapping("/getAllUsers")
	public Iterable<Usuario> getAllUsers () {
		
		Iterable<Usuario> findAll = usuarioRepository.findAll();
		
		return findAll;
		
	}
	
	@RequestMapping(value="/addUser", method = RequestMethod.POST)
    public String añadirUsuario(@RequestBody Usuario usuario) {//REST Endpoint.
    	usuarioRepository.save(usuario);
    	return"Usuario Insertado";
    }

	
	@RequestMapping ("/getUserById")
	public Usuario getUserById (@RequestParam long userId) {
		return usuarioRepository.findById(userId);	
	}
}
