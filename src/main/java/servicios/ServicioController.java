package servicios;

import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.poli.devweb.model.entities.Consultor;
import com.poli.devweb.model.entities.Servicio;
import com.poli.devweb.model.entities.Usuario;
import com.poli.devweb.repositories.ServicioRepository;
import com.poli.devweb.repositories.UsuarioRepository;
@RestController
public class ServicioController {
	
	@Autowired
	private ServicioRepository servicioRepository;

	@RequestMapping(value="/addServicio", method = RequestMethod.POST)
    public Set<Servicio> consultarServiciosPorConsultor(@RequestBody Consultor consultor) {//REST Endpoint.
		return servicioRepository.findByConsultor(consultor);
    }

	
}
