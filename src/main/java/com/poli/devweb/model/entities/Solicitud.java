package com.poli.devweb.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name="solicitud")
public class Solicitud {
	
	@Id
	private Long idSolicitud;
	private long idServicio;
	private long idCotizacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario", nullable = false)
	private Usuario usuario;	
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(unique = true)
    private Cotizacion cotizacion;
	
	public long getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}
	public long getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public long getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Cotizacion getCotizacion() {
		return cotizacion;
	}
	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}
	public void setIdServicio(long idServicio) {
		this.idServicio = idServicio;
	}
	
}
