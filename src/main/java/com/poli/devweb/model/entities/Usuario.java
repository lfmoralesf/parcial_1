package com.poli.devweb.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Usuario")
public class Usuario {
	
    @Id
    @Column(name="id_usuario")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id_usuario;

    private String name;

    private String perfil;
    
    @OneToMany (fetch=FetchType.LAZY, mappedBy="usuario")
    private Set<Cotizacion> cotizaciones = new HashSet<Cotizacion> (0); 
     
    @OneToMany (fetch=FetchType.LAZY, mappedBy="usuario")
    private Set<Cotizacion> solicitud = new HashSet<Cotizacion> (0); 
     
    
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public Set<Cotizacion> getCotizaciones() {
		return cotizaciones;
	}

	public void setCotizaciones(Set<Cotizacion> cotizaciones) {
		this.cotizaciones = cotizaciones;
	}

}