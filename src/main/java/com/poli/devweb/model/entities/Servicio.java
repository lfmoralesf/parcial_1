package com.poli.devweb.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="servicio")
public class Servicio {

	@Id
	@Column(name="id_servicio")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idServicio;
	private long idCategoria;
	private long idCotizacion;
	private String descripcion;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="idCategoria")
	private Set<Categoria> categoria;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="idCotizacion")
	private Set<Cotizacion> cotizacion;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_consultor", nullable = false)
	private Consultor consultor;
	
	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}

	
	
	public long getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public Set<Cotizacion> getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(Set<Cotizacion> cotizacion) {
		this.cotizacion = cotizacion;
	}

	public long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public Set<Categoria> getCategoria() {
		return categoria;
	}

	public void setCategoria(Set<Categoria> categoria) {
		this.categoria = categoria;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


    
	public long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(long idServicio) {
		this.idServicio = idServicio;
	}


}
