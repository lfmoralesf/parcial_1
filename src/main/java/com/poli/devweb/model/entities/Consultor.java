package com.poli.devweb.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="consultor")
public class Consultor {

	@Id
	@Column(name="id_consultor")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long  idConsultor;
	private String nombre;
	
    @OneToMany (fetch=FetchType.LAZY, mappedBy="consultor")
    private Set<Servicio> servicios= new HashSet<Servicio> (0);
    
	public Set<Servicio> getServicios() {
		return servicios;
	}


	public void setServicios(Set<Servicio> servicios) {
		this.servicios = servicios;
	}


	public long getIdConsultor() {
		return idConsultor;
	}


	public void setIdConsultor(long idConsultor) {
		this.idConsultor = idConsultor;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}





}
