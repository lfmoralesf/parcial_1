package com.poli.devweb.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poli.devweb.model.entities.Consultor;
import com.poli.devweb.model.entities.Cotizacion;
import com.poli.devweb.model.entities.Servicio;
import com.poli.devweb.model.entities.Usuario;

@Repository
public interface CotizacionRepository extends CrudRepository<Cotizacion, Long> {

	Cotizacion findByConsultor(Consultor consultor);

	Cotizacion findByServicio(Servicio servicio);

}
