package com.poli.devweb.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poli.devweb.model.entities.Categoria;
import com.poli.devweb.model.entities.Solicitud;
import com.poli.devweb.model.entities.Usuario;

@Repository
public interface SolicitudRepository extends CrudRepository<Solicitud, Long> {
	

}
