package com.poli.devweb.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.poli.devweb.model.entities.Consultor;
import com.poli.devweb.model.entities.Servicio;
import com.poli.devweb.model.entities.Usuario;

@Repository
public interface ServicioRepository extends CrudRepository<Servicio, Long> {

	public Set<Servicio> findByConsultor(Consultor consultor);
}
